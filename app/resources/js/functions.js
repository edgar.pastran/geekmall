// Funcion que permite obtener el valor de un parametro
// presente en la URL (Query String)
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

// Funcion usada en la validacion de formularios
// con JQuery Validation
// Necessary to place dyncamic error messages
// without breaking the expected markup for custom input
function errorPlacementInput(error, element) {
    if (element.is(':radio') || element.is(':checkbox')) {
        if (element.closest('.custom-button').length) {
            error.insertAfter(element.closest('.custom-button'));
        }
        else {
            error.insertAfter(element.parent());
        }
    }
    else if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
    }
    else {
        error.insertAfter(element);
    }
}

// Funcion que devuelve un boolean indicando si
// el cliente esta usando Microsof Internet Explorer
function msie() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));
}

function myCheckbox(myCheck, valor) {
    var checkBox = document.getElementById(myCheck);
    if (checkBox.checked == true) {
        checkBox.value = valor;
    } else {
        checkBox.value = "";
    }
}

function setCheckbox(myCheck, valor) {
    var checkBox = document.getElementById(myCheck);
    if (checkBox.value == valor) {
        checkBox.checked = true;
    } else {
        checkBox.checked = false;
    }
}

function fix_xeditable_conflict() {
    $('.datepicker > div:first-of-type').css('display', 'initial');
}
