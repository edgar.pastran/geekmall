var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.setUser");

(function() {
    'use strict';
    $(userSignup);
})();

/**************************************************************************
 * VALIDACION DE FORMULARIO DE REGISTRO - INICIO
 **************************************************************************/
function userSignup() {
    $('#birthday').datepicker({
        format: 'dd/mm/yyyy',
        startView: 'years',
        startDate: '-100y',
        endDate: '-12y',
        autoclose: true,
        container: '#birthday-datepicker-container'
    });

    if (!$.fn.validate) return;

    var $form = $('#user-signup');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            firstname: {
                required: true,
                rangelength: [2, 50]
            },
            lastname: {
                required: true,
                rangelength: [2, 50]
            },
            birthday: {
                required: true,
                dateITA: true
            },
            gender: {
                required: true,
            },
            haveson: {
                required: true,
            },
            username: {
                required: true,
                email: true,
                rangelength: [6, 50]
            },
            password: {
                required: true,
                rangelength: [6, 20]

            },
            password_confirm: {
                required: true,
                rangelength: [6, 20],
                equalTo: '#password'
            }
        },
        submitHandler: function() {
            signup();
        }
    });
}

function signup() {
    var usuario = {};
    usuario.id_usuario = $('#username').val();
    usuario.clave = $('#password').val();
    usuario.correo = $('#username').val();
    usuario.nombres = $('#firstname').val();
    usuario.apellidos = $('#lastname').val();
    usuario.fecha_nacimiento = moment($('#birthday').datepicker("getDate")).format("YYYY-MM-DD");
    usuario.genero = $('input[name=gender]:checked').val();
    usuario.posee_hijos = $('#haveson').val();
    var params = {
        "data": JSON.stringify({
            "client": "23C59575B9CF467C9620760EB255B389",
            "organization": idOrganization,
            "active": true,
            "searchKey": usuario.nombres+" "+usuario.apellidos,
            "name": usuario.nombres+" "+usuario.apellidos,
            "businessPartnerCategory": "713643BF6582479BAD2AF21B80836AA2",
            "wmFnamevisitor": usuario.nombres,
            "wmLnamevisitor": usuario.apellidos,
            "wmBirthdatevisitor": usuario.fecha_nacimiento,
            "wmGenerevisitor": usuario.genero,
            "wmHaveson": usuario.posee_hijos,
            "wmIsvisitor": "Y",
            "wmUsermail": usuario.correo,
            "wmUserpass": hex_md5(usuario.clave)
        })
    };
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        dataType:"text",
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            var respuesta = JSON.parse(respuesta);
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            if (respuesta.status) {
                swal({
                    title: 'Usuario creado',
                    text: 'Ya puedes iniciar sesion con tu usuario',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: false
                },
                function () {
                    document.location.href = urlClient.concat("login.html");
                });
            }
            else {
                swal(respuesta.message, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * VALIDACION DE FORMULARIO DE REGISTRO - FIN
 **************************************************************************/
