var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.getStoreList");

$(function() {
   loadStores();
   $('.filter-text').val('');
});

function loadStores() {
    var params = {};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            $('#stores').html('');
            if (respuesta.hasOwnProperty('store')) {
                for (i = 0; i < respuesta.store.length; i++) {
                    var store = respuesta.store[i];
                    loadStore(store);
                }
                initIsotope();
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadStore(store) {
    var row =
        '<div id="'+store.id+'" class="element initially-hidden col-md-4 col-lg-4" title="'+store.name+'" data-category="'+store.name+'">' +
            '<div class="custom-button">' +
                '<div class="store-name hidden">'+store.name.toLowerCase()+'</div>' +
                '<p style="padding: 2px;">'+store.name+'</p>' +
            '</div>' +
        '</div>';
    $('#stores').append(row);

    $('.element').click(function() {
        var id_store = $(this).attr('id');
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"store.html?id_store="+id_store);
    });
}

function initIsotope() {
    var $container = $('.elements');

    // init Isotope
    $container.isotope({
        itemSelector: '.element',
        layoutMode: 'fitRows',
        filter: ':not(.initially-hidden)'
    });

    var PageLoadFilter = ':not(.initially-hidden)';
    $container.isotope({filter: PageLoadFilter});

    // filter functions
    $('.filter-text').keyup(function () {
        var filterValue = $(this).val().trim();
        $container.isotope({filter: function() {
            if (filterValue == "") {
                return false;
            }
            else {
                var store_name = $(this).find('.store-name').text();
                return store_name.indexOf(filterValue.toLowerCase()) >= 0;
            }
        }});
    });
}
