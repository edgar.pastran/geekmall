var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.getPromoDetail");

$(function() {
   checkParameters();
});

function loadPromotion(id_promotion) {
    var usuario = getUserData();
    var params = {"idPromotion": id_promotion, "idUser": usuario.id_usuario};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            showPromotion(respuesta.promociones[0]);
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showPromotion(promotion) {
    $('#store_name').html(promotion.nameStore);
    $('#store_image').attr('src', promotion.imageURL);

    moment.locale('es');
    var timeEndDate = moment(promotion.dateEnd).format("h:mm a");
    var dayEndDate = moment(promotion.dateEnd).format("DD");
    var monthEndDate = moment(promotion.dateEnd).format("MMMM").toUpperCase();
    var yearEndDate = moment(promotion.dateEnd).format("YYYY");

    $('#timeEndDate').html(timeEndDate);
    $('#dayEndDate').html(dayEndDate);
    $('#monthEndDate').html(monthEndDate);
    $('#yearEndDate').html(yearEndDate);

    $('#greeting').html(promotion.greeting);
    $('#namePromotion').html(promotion.namePromotion);
    $('#description').html(promotion.description);
}

function checkParameters() {
    var id_promotion = getUrlParameter('id_promotion');
    if (id_promotion != '') {
        loadPromotion(id_promotion);
    }
    else {
        window.history.back();
    }
}
