var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.getCateBrand");

$(function() {
   loadCategoriesAndBrands();
});

function loadCategoriesAndBrands() {
    var params = {};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            if (respuesta.hasOwnProperty('categorias')) {
                loadCategories(respuesta.categorias);
            }
            if (respuesta.hasOwnProperty('marcas')) {
                loadBrands(respuesta.marcas);
            }
            $("#accordion_categories_preferences").accordion({
                active: false,
                collapsible: true,
                heightStyle: "content"
            });
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadCategories(categories) {
    $('#accordion_categories_preferences').html('');
    for (i=0; i<categories.length; i++) {
        var category = categories[i];
        loadCategory(category);
    }

    // Select the user's preferences
    var usuario = getUserData();
    usuario.preferencias.categorias.forEach(function(item) {
        $('#'+item).click();
    });
}

function loadCategory(category) {
    // console.log(JSON.stringify(category));
    var row =
        '<div class="text-center background-color-3 mt-2 p-1">'+
            '<span class="nav-icon">' +
                '<em class="'+category.icono+' mr-2" style="font-size: 16px;"></em>' +
            '</span>'+
            '<span>'+category.name.toUpperCase()+'</span>'+
        '</div>'+
        '<div class="subcategories row">';
    if (category.hasOwnProperty('subcategorias')) {
        for (j=0; j<category.subcategorias.length; j++) {
            var subcategory = category.subcategorias[j];
            row +=
                '<div class="col-md-4 col-lg-4 p-0" title="'+subcategory.name+'">' +
                    '<div id="'+subcategory.id+'" class="custom-button">' +
                        // '<img src="'+subcategory.imageURL+'">' +
                        '<p>'+subcategory.name+'</p>' +
                        '<label class="custom-control custom-checkbox hidden">' +
                            '<input class="custom-control-input adviser-radio" id="subcategory-'+subcategory.id+'" name="subcategory-'+subcategory.id+'" type="checkbox">' +
                            '<span class="custom-control-indicator"></span>' +
                        '</label>' +
                    '</div>' +
                '</div>';
        }
    }
    row += '</div>';
    $('#accordion_categories_preferences').append(row);
}

function loadBrands(brands) {
    var row =
        '<div class="text-center background-color-3 mt-2 p-1">'+
            '<span class="nav-icon">' +
                '<em class="ion-social-markdown mr-2" style="font-size: 16px;"></em>' +
            '</span>'+
            '<span>MARCAS</span>'+
        '</div>'+
        '<div class="brands row">';
    for (i=0; i<brands.length; i++) {
        var brand = brands[i]
        row +=
            '<div class="col-md-4 col-lg-4 p-0" title="'+brand.name+'">' +
                '<div id="'+brand.id+'" class="custom-button">' +
                    '<p>'+brand.name+'</p>' +
                    '<label class="custom-control custom-checkbox hidden">' +
                        '<input class="custom-control-input adviser-radio" id="brand-'+brand.id+'" name="brand-'+brand.id+'" type="checkbox">' +
                        '<span class="custom-control-indicator"></span>' +
                    '</label>' +
                '</div>' +
            '</div>';
    }
    row += '</div>';
    $('#accordion_categories_preferences').append(row);

    // Select the user's preferences
    var usuario = getUserData();
    usuario.preferencias.marcas.forEach(function(item) {
        $('#'+item).click();
    });
}

function save() {
    var categories = $('.subcategories').find('.selected');
    var selectedCategories = [];
    var userCategories = [];
    for (i=0; i<categories.length; i++) {
        var idCategory = $(categories[i]).attr('id');
        selectedCategories.push({idCategory: idCategory});
        userCategories.push(idCategory);
    }

    var brands = $('.brands').find('.selected');
    var selectedBrands = [];
    var userBrands = [];
    for (i=0; i<brands.length; i++) {
        var idBrand = $(brands[i]).attr('id');
        selectedBrands.push({idBrand: idBrand});
        userBrands.push(idBrand);
    }

    var urlServerPage2 = urlServer.concat("ws/co.openmas.wokemall.setPreference");
    var usuario = getUserData();
    var params ={
        "data": JSON.stringify({
            idOrg: idOrganization,
            idUser: usuario.id_usuario,
            category: selectedCategories,
            brand: selectedBrands
        })
    };
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage2,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            if (respuesta.status) {
                var usuario = getUserData();
                if (usuario != null) {
                    usuario.preferencias.categorias = userCategories;
                    usuario.preferencias.marcas = userBrands;
                    setUserData(usuario);
                }
                swal({
                    title: 'Sus preferencias han sido actualizadas',
                    text: '',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    document.location.reload();
                });
            }
            else {
                swal(respuesta.message, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

$('#btn-save').click(function() {
    var categories = $('.subcategories').find('.selected');
    var brands = $('.brands').find('.selected');
    if ((categories.length > 0) || (brands.length > 0)){
        save();
    }
    else {
        swal('Debes seleccionar al menos una preferencia', '', 'error');
    }
});
