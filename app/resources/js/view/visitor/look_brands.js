var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.getStoreBrand");

$(function() {
   loadData();
});

function loadData() {
    var params = {};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            // Brands
            $('#brands').html('');
            if (respuesta.hasOwnProperty('brand')) {
                loadBrands(respuesta.brand);
            }
            // Stores
            $('#stores').html('');
            if (respuesta.hasOwnProperty('store')) {
                loadStores(respuesta.store);
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadBrands(brands) {
    for (i=0; i<brands.length; i++) {
        var brand = brands[i]
        loadBrand(brand);
    }
}

function loadBrand(brand) {
    var row =
        '<div class="element-parent col-md-4 col-lg-4 p-0" title="'+brand.name+'">' +
            '<div id="'+brand.id+'" class="custom-button filter-brand">' +
                '<div class="brand-name hidden">'+brand.name.toLowerCase()+'</div>' +
                '<p>'+brand.name+'</p>' +
                '<label class="custom-control custom-checkbox hidden">' +
                '<input class="custom-control-input adviser-radio" id="brand-'+brand.id+'" name="brand-'+brand.id+'" type="checkbox">' +
                '<span class="custom-control-indicator"></span>' +
                '</label>' +
            '</div>' +
        '</div>';
    $('#brands').append(row);
}

function loadStores(stores) {
    for (i = 0; i < stores.length; i++) {
        var store = stores[i];
        loadStore(store);
    }
    initIsotopeBrands();
    initIsotopeStores();
}

function loadStore(store) {
    var brands = "";
    for (b=0; b<store.storeBrands.length; b++) {
        var brand = store.storeBrands[b];
        brands += ((brands=="")?"":" ")+brand.idBrand;
    }
    var row =
        '<div id="'+store.id+'" class="element initially-hidden col-md-4 col-lg-4 '+brands+'" title="'+store.name+'" data-category="'+brands+'">' +
            '<div class="custom-button">' +
                // '<img src="'+store.image+'">' +
                '<p style="padding: 2px;">'+store.name+'</p>' +
            '</div>' +
        '</div>';
    $('#stores').append(row);

    $('.element').click(function() {
        var id_store = $(this).attr('id');
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"store.html?id_store="+id_store);
    });
}

function initIsotopeBrands() {
    var $container = $('.elements-parent');

    // init Isotope
    $container.isotope({
        itemSelector: '.element-parent',
        layoutMode: 'fitRows',
        filter: ':not(.initially-hidden)'
    });

    var PageLoadFilter = ':not(.initially-hidden)';
    $container.isotope({filter: PageLoadFilter});

    // filter functions
    $('.filter-text').keyup(function () {
        var filterValue = $(this).val().trim();
        $container.isotope({filter: function() {
            var brand_name = $(this).find('.brand-name').text();
            return brand_name.indexOf(filterValue.toLowerCase()) >= 0;
        }});
    });
}

function initIsotopeStores() {
    var $container = $('.elements');

    // init Isotope
    $container.isotope({
        itemSelector: '.element',
        layoutMode: 'fitRows',
        filter: ':not(.initially-hidden)'
    });

    var PageLoadFilter = ':not(.initially-hidden)';
    $container.isotope({filter: PageLoadFilter});

    // filter functions
    $('.filter-brand').click(function () {
        var selected = $('#brands').find('.selected');
        var brands = [];
        for (var f=0; f<selected.length; f++) {
            var id = "."+$(selected[f]).attr('id');
            brands.push(id);
        }
        var ownID = "."+$(this).attr('id');
        if (!$(this).is('.selected')) {
            brands.push(ownID);
        }
        else {
            brands = jQuery.grep(brands, function(value) {
                return value != ownID;
            });
        }
        brands = brands.join(', ');
        $container.isotope({filter: function() {
            return $(this).is(brands);
        }});
    });
}
