var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.getPromoCate");

$(function() {
   loadData();
    $('.filter-text').val('');
});

function loadData() {
    var usuario = getUserData();
    var params = {"idUser": usuario.id_usuario};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            // Categories
            if (respuesta.hasOwnProperty('category')) {
                loadCategories(respuesta.category);
                $("#accordion_categories").accordion({
                    active: false,
                    collapsible: true,
                    heightStyle: "content"
                });
            }
            // Promotions
            $('#promotions').html('');
            if (respuesta.hasOwnProperty('promotion')) {
                loadPromotions(respuesta.promotion);
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadCategories(categories) {
    $('#accordion_categories').html('');
    for (i=0; i<categories.length; i++) {
        var category = categories[i];
        loadCategory(category);
    }
}

function loadCategory(category) {
    var row =
        '<div class="text-center background-color-3 mt-2 p-1">'+
            '<span>'+category.name.toUpperCase()+'</span>'+
        '</div>'+
        '<div class="subcategories row">';
    if (category.hasOwnProperty('subcategorias')) {
        for (j=0; j<category.subcategorias.length; j++) {
            var subcategory = category.subcategorias[j];
            row +=
                '<div class="col-md-4 col-lg-4 p-0" title="'+subcategory.name+'">' +
                    '<div id="'+subcategory.id+'" class="custom-button filter-category">' +
                        // '<img src="'+subcategory.imageURL+'">' +
                        '<p>'+subcategory.name+'</p>' +
                        '<label class="custom-control custom-checkbox hidden">' +
                            '<input class="custom-control-input adviser-radio" id="subcategory-'+subcategory.id+'" name="subcategory-'+subcategory.id+'" type="checkbox">' +
                            '<span class="custom-control-indicator"></span>' +
                        '</label>' +
                    '</div>' +
                '</div>';
        }
    }
    row += '</div>';
    $('#accordion_categories').append(row);
}

function loadPromotions(promotions) {
    for (i = 0; i < promotions.length; i++) {
        var promotion = promotions[i];
        loadPromotion(promotion);
    }
    initIsotope();
}

function loadPromotion(promotion) {
    // console.log(JSON.stringify(promotion));
    moment.locale('es');
    var dayStartDate = moment(promotion.datStaProm).format("DD");
    var monthStartDate = moment(promotion.datStaProm).format("MMM").toUpperCase();
    var yearStartDate = moment(promotion.datStaProm).format("YYYY");
    var dayEndDate = moment(promotion.datEndProm).format("DD");
    var monthEndDate = moment(promotion.datEndProm).format("MMM").toUpperCase();
    var yearEndDate = moment(promotion.datEndProm).format("YYYY");

    var usuario = getUserData();
    var url_redirection = urlClient.concat(usuario.directorio_vistas+"store.html?id_store="+promotion.idStore);

    var row =
        '<div id="'+promotion.id+'" class="element initially-hidden col-md-4 col-lg-4 '+promotion.idCategory+'" title="'+promotion.name+'" data-category="'+promotion.idCategory+'">' +
            '<div class="custom-button">' +
                '<div class="promotion-id hidden">' +
                    promotion.id +
                '</div>' +
                '<div class="promotion-greeting hidden">' +
                    promotion.greeting +
                '</div>' +
                '<div class="promotion-name hidden">' +
                    promotion.name +
                '</div>' +
                '<div class="promotion-description hidden">' +
                    promotion.descProm +
                '</div>' +
                '<div class="promotion-start-date hidden">' +
                    dayStartDate+' '+monthStartDate+' '+yearStartDate +
                '</div>' +
                '<div class="promotion-end-date hidden">' +
                    dayEndDate+' '+monthEndDate+' '+yearEndDate +
                '</div>' +
                '<div class="promotion-url-redirection hidden">' +
                    url_redirection +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-2">' +
                        '<img src="'+promotion.logoStore+'" alt="'+promotion.nameStore+'" class="fw promotion-image-store">' +
                    '</div>' +
                    '<div class="col-10 text-left text-md promotion-name-store">' +
                        promotion.nameStore +
                    '</div>' +
                '</div>' +
                '<div>' +
                    '<img src="'+promotion.imageURL+'" alt="'+promotion.name+'" class="fw promotion-image">' +
                    '<div class="keyword hidden">'+promotion.name.toLowerCase()+'</div>' +
                '</div>' +
                '<div class="text-center background-lightgray p-2">'+
                    '<span>Ver m&aacute;s</span>'+
                '</div>' +
            '</div>' +
        '</div>';
    $('#promotions').append(row);

    $('.element').click(function() {
        openPromotion($(this));
    });
}

function openPromotion(promotion) {
    // Open modal
    $('#promotion_image_store').attr("src", $(promotion).find('.promotion-image-store').attr("src"));
    $('#promotion_name_store').html($(promotion).find('.promotion-name-store').html());
    $('#promotion_image').attr("src", $(promotion).find('.promotion-image').attr("src"));
    $('#promotion_greeting').html($(promotion).find('.promotion-greeting').html());
    $('#promotion_name').html($(promotion).find('.promotion-name').html());
    $('#promotion_start_date').html($(promotion).find('.promotion-start-date').html());
    $('#promotion_end_date').html($(promotion).find('.promotion-end-date').html());
    $('#promotion_title').html($(promotion).find('.promotion-name').html());
    $('#promotion_description').html($(promotion).find('.promotion-description').html());
    var url_redirection = $(promotion).find('.promotion-url-redirection').html();
    $('.modal-promotion').find('.modal-footer').hide();
    if (url_redirection != '') {
        $('#btn_promotion_details').click(function() {
            document.location.href = url_redirection;
        });
        $('.modal-promotion').find('.modal-footer').show();
    }
    $('.modal-promotion').modal();
}

function initIsotope() {
    var $container = $('.elements');

    // init Isotope
    $container.isotope({
        itemSelector: '.element',
        layoutMode: 'fitRows',
        filter: ':not(.initially-hidden)'
    });

    var PageLoadFilter = ':not(.initially-hidden)';
    $container.isotope({filter: PageLoadFilter});

    // layout Isotope after each image loads
    $container.imagesLoaded().progress( function() {
        $container.isotope('layout');
    });

    // filter functions
    $('.filter-category').click(function () {
        var selected = $('#accordion_categories').find('.selected');
        var categories = [];
        for (var f=0; f<selected.length; f++) {
            var id = "."+$(selected[f]).attr('id');
            categories.push(id);
        }
        var ownID = "."+$(this).attr('id');
        if (!$(this).is('.selected')) {
            categories.push(ownID);
        }
        else {
            categories = jQuery.grep(categories, function(value) {
                return value != ownID;
            });
        }
        categories = categories.join(', ');
        //console.log("CATEGORIES: "+categories);
        applyIsotopeFilters(categories, null);
    });

    $('.filter-text').keyup(function () {
        var filterValue = $(this).val();
        applyIsotopeFilters(null, filterValue);
    });
    $('.filter-text').change(function () {
        var filterValue = $(this).val();
        applyIsotopeFilters(null, filterValue);
    });
}

function applyIsotopeFilters(categories, text) {
    if (categories == null) {
        var selected = $('#accordion_categories').find('.selected');
        var categories = [];
        for (var f=0; f<selected.length; f++) {
            var id = "."+$(selected[f]).attr('id');
            categories.push(id);
        }
        categories = categories.join(', ');
    }
    if (text == null) {
        text = $('.filter-text').val();
    }
    text = text.toLowerCase().trim();

    var $container = $('.elements');
    $container.isotope({filter: function() {
        var keyword = $(this).find('.keyword').text();
        var is_ok_for_text = (text != "")?keyword.indexOf(text) >= 0:(categories != ""?true:false);
        var is_ok_for_categories = (categories != "")?$(this).is(categories):(text != ""?true:false);
        return is_ok_for_text && is_ok_for_categories;
    }});
}