var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.getStore");

$(function() {
   checkParameters();
});

function loadStore(id_store) {
    var usuario = getUserData();
    var params = {
        "idStore": id_store,
        "idUser": usuario.id_usuario
    };
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            console.log("RESPUESTA: "+JSON.stringify(respuesta));
            showStore(respuesta);
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showStore(store) {
    $('#store_name').html(store.name);
    $('#store_image').attr('src', store.image);
    $('#store_image_facade').attr('src', store.imageFace);
    $('#store_image_map').attr('src', store.imageMap);
    $('#store_level_number').html(store.level);
    $('#store_local_number').html(store.localNumber);
    $('#store_schedule_start').html(store.startTime);
    $('#store_schedule_end').html(store.endTime);
    // Promotions
    loadPromotions(store.promoStore);
    // Twitter
    $('#store_twitter').click(function() {
        if (store.twitter != '') {
            window.open(store.twitter, '_blank');
        }
    });
    // Facebook
    $('#store_facebook').click(function() {
        if (store.facebook != '') {
            window.open(store.facebook, '_blank');
        }
    });
    // Instagram
    $('#store_instagram').click(function() {
        if (store.instagram != '') {
            window.open(store.instagram, '_blank');
        }
    });
    // Web
    $('#store_web_page').click(function() {
        if (store.webpage != '') {
            window.open(store.webpage, '_blank');
        }
    });
}

function loadPromotions(promotions) {
    for (i = 0; i < promotions.length; i++) {
        var promotion = promotions[i];
        loadPromotion(promotion);
    }
}

function loadPromotion(promotion) {
    // console.log(JSON.stringify(promotion));
    moment.locale('es');
    var dayStartDate = moment(promotion.datStaProm).format("DD");
    var monthStartDate = moment(promotion.datStaProm).format("MMM").toUpperCase();
    var yearStartDate = moment(promotion.datStaProm).format("YYYY");
    var dayEndDate = moment(promotion.datEndProm).format("DD");
    var monthEndDate = moment(promotion.datEndProm).format("MMM").toUpperCase();
    var yearEndDate = moment(promotion.datEndProm).format("YYYY");

    var usuario = getUserData();
    var url_redirection = urlClient.concat(usuario.directorio_vistas+"store.html?id_store="+promotion.idStore);

    var row =
        '<div id="'+promotion.idPromotion+'" class="element initially-hidden col-md-4 col-lg-4" title="'+promotion.namePromo+'" data-category="'+promotion.idCategory+'">' +
            '<div class="custom-button">' +
                '<div class="promotion-id hidden">' +
                    promotion.idPromotion +
                '</div>' +
                '<div class="promotion-greeting hidden">' +
                    promotion.greeting +
                '</div>' +
                '<div class="promotion-name hidden">' +
                    promotion.namePromo +
                '</div>' +
                '<div class="promotion-description hidden">' +
                    promotion.descPromo +
                '</div>' +
                '<div class="promotion-start-date hidden">' +
                    dayStartDate+' '+monthStartDate+' '+yearStartDate +
                '</div>' +
                '<div class="promotion-end-date hidden">' +
                    dayEndDate+' '+monthEndDate+' '+yearEndDate +
                '</div>' +
                '<div class="promotion-url-redirection hidden">' +
                    url_redirection +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-2">' +
                        '<img src="'+promotion.imageStore+'" alt="'+promotion.nameStore+'" class="fw promotion-image-store">' +
                    '</div>' +
                    '<div class="col-10 text-left text-md promotion-name-store">' +
                    promotion.nameStore +
                    '</div>' +
                '</div>' +
                '<div>' +
                    '<img src="'+promotion.imageURL+'" alt="'+promotion.namePromo+'" class="fw promotion-image">' +
                    '<div class="keyword hidden">'+promotion.namePromo.toLowerCase()+'</div>' +
                '</div>' +
                '<div class="text-center background-lightgray p-2">'+
                    '<span>Ver m&aacute;s</span>'+
                '</div>' +
            '</div>' +
        '</div>';
    $('#promotions').append(row);

    $('.element').click(function() {
        openPromotion($(this));
    });
}

function openPromotion(promotion) {
    // Open modal
    $('#promotion_image_store').attr("src", $(promotion).find('.promotion-image-store').attr("src"));
    $('#promotion_name_store').html($(promotion).find('.promotion-name-store').html());
    $('#promotion_image').attr("src", $(promotion).find('.promotion-image').attr("src"));
    $('#promotion_greeting').html($(promotion).find('.promotion-greeting').html());
    $('#promotion_name').html($(promotion).find('.promotion-name').html());
    $('#promotion_start_date').html($(promotion).find('.promotion-start-date').html());
    $('#promotion_end_date').html($(promotion).find('.promotion-end-date').html());
    $('#promotion_title').html($(promotion).find('.promotion-name').html());
    $('#promotion_description').html($(promotion).find('.promotion-description').html());
    var url_redirection = $(promotion).find('.promotion-url-redirection').html();
    $('.modal-promotion').find('.modal-footer').hide();
    if (url_redirection != '') {
        $('#btn_promotion_details').click(function() {
            document.location.href = url_redirection;
        });
        $('.modal-promotion').find('.modal-footer').show();
    }
    $('.modal-promotion').modal();
}

function checkParameters() {
    var id_store = getUrlParameter('id_store');
    if (id_store != '') {
        loadStore(id_store);
    }
    else {
        window.history.back();
    }
}
