var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.getStoreBrandProms");

$(function() {
    $('.filter-text').val('');
    loadData();
});

function loadData() {
    var usuario = getUserData();
    var params = {"idUser": usuario.id_usuario};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            $('#elements').html('');
            // Stores
            if (respuesta.hasOwnProperty('store')) {
                for (var s=0; s<respuesta.store.length; s++) {
                    var store = respuesta.store[s];
                    loadStore(store);
                }
            }
            // Brands
            if (respuesta.hasOwnProperty('brand')) {
                for (var b=0; b<respuesta.brand.length; b++) {
                    var brand = respuesta.brand[b];
                    loadBrand(brand);
                }
            }
            // Promotions
            if (respuesta.hasOwnProperty('promo')) {
                for (var p=0; p<respuesta.promo.length; p++) {
                    var promotion = respuesta.promo[p];
                    loadPromotion(promotion);
                }
            }
            initIsotope();
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadStore(store) {
    var row =
        '<div id="'+store.id+'" class="element store initially-hidden col-md-4 col-lg-4 pl-0 pr-0 pt-2" title="'+store.name+'">' +
            '<div class="custom-button m-0">' +
                '<div class="row">' +
                    '<div class="col-2">' +
                        '<img src="'+store.imageLogo+'" alt="'+store.name+'" class="fw">' +
                    '</div>' +
                    '<div class="col-10 text-left text-md">' +
                        store.name +
                    '</div>' +
                '</div>' +
                '<div>' +
                    '<img src="'+store.imageFace+'" alt="'+store.name+'" class="fw">' +
                    '<div class="keyword hidden">'+store.name.toLowerCase()+'</div>' +
                '</div>' +
                '<div class="text-center background-lightgray p-2">'+
                    '<span>Ver m&aacute;s</span>'+
                '</div>' +
            '</div>' +
        '</div>';
    $('#elements_container').append(row);

    $('#'+store.id).click(function() {
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"store.html?id_store="+store.id);
    });
}

function loadBrand(brand) {
    var row =
        '<div id="'+brand.id+'" class="element brand initially-hidden col-md-4 col-lg-4 pl-0 pr-0 pt-2" title="'+brand.name+'">' +
            '<div class="custom-button m-0">' +
                '<div class="brand-stores hidden">' +
                    JSON.stringify(brand.storeBrands) +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-2">' +
                        '<img src="'+brand.image+'" alt="'+brand.name+'" class="fw brand-image">' +
                    '</div>' +
                    '<div class="col-10 text-left text-md brand-name">' +
                        brand.name +
                    '</div>' +
                '</div>' +
                '<div>' +
                    '<img src="'+brand.image+'" alt="'+brand.name+'" class="fw brand-image">' +
                    '<div class="keyword hidden">'+brand.name.toLowerCase()+'</div>' +
                '</div>' +
                '<div class="text-center background-lightgray p-2">'+
                    '<span>Ver m&aacute;s</span>'+
                '</div>' +
            '</div>' +
        '</div>';
    $('#elements_container').append(row);

    $('.brand').click(function() {
        openBrand($(this));
    });
}

function openBrand(brand) {
    // Open modal
    var id_brand = $(brand).attr('id');
    $('#brand_image_header').attr("src", $(brand).find('.brand-image').attr("src"));
    $('#brand_name').html($(brand).find('.brand-name').html());
    $('#brand_image_body').attr("src", $(brand).find('.brand-image').attr("src"));
    $('#brand_stores').html('');

    var stores = JSON.parse($(brand).find('.brand-stores').html());
    for (var s=0; s<stores.length; s++) {
        var store = stores[s];
        var row =
            '<div id="'+id_brand+'-'+store.idStore+'" class="col-md-4 col-lg-4 pt-2 pl-0 pr-0" title="'+store.nameStore+'">' +
                '<div class="custom-button m-0">' +
                    '<div class="store-id hidden">'+store.idStore+'</div>' +
                    '<p class="p-0">'+store.nameStore+'</p>' +
                '</div>' +
            '</div>';
        $('#brand_stores').append(row);

        $('#'+id_brand+'-'+store.idStore).click(function() {
            var id_store = $(this).find('.store-id').html();
            var usuario = getUserData();
            document.location.href = urlClient.concat(usuario.directorio_vistas+"store.html?id_store="+id_store);
        });
    }
    $('.modal-brand').modal();
}

function loadPromotion(promotion) {
    moment.locale('es');
    var dayStartDate = moment(promotion.dateStart).format("DD");
    var monthStartDate = moment(promotion.dateStart).format("MMM").toUpperCase();
    var yearStartDate = moment(promotion.dateStart).format("YYYY");
    var dayEndDate = moment(promotion.dateEnd).format("DD");
    var monthEndDate = moment(promotion.dateEnd).format("MMM").toUpperCase();
    var yearEndDate = moment(promotion.dateEnd).format("YYYY");

    var usuario = getUserData();
    var url_redirection = urlClient.concat(usuario.directorio_vistas+"store.html?id_store="+promotion.idStore);

    var row =
        '<div id="'+promotion.idPromotion+'" class="element promotion initially-hidden col-md-4 col-lg-4 pl-0 pr-0 pt-2" title="'+promotion.namePromo+'">' +
            '<div class="custom-button m-0">' +
                '<div class="promotion-id hidden">' +
                    promotion.idPromotion +
                '</div>' +
                '<div class="promotion-greeting hidden">' +
                    promotion.greeting +
                '</div>' +
                '<div class="promotion-name hidden">' +
                    promotion.namePromo +
                '</div>' +
                '<div class="promotion-description hidden">' +
                    promotion.descPromo +
                '</div>' +
                '<div class="promotion-start-date hidden">' +
                    dayStartDate+' '+monthStartDate+' '+yearStartDate +
                '</div>' +
                '<div class="promotion-end-date hidden">' +
                    dayEndDate+' '+monthEndDate+' '+yearEndDate +
                '</div>' +
                '<div class="promotion-url-redirection hidden">' +
                    url_redirection +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-2">' +
                        '<img src="'+promotion.imageStore+'" alt="'+promotion.nameStore+'" class="fw promotion-image-store">' +
                    '</div>' +
                    '<div class="col-10 text-left text-md promotion-name-store">' +
                        promotion.nameStore +
                    '</div>' +
                '</div>' +
                '<div>' +
                    '<img src="'+promotion.imageURL+'" alt="'+promotion.namePromo+'" class="fw promotion-image">' +
                    '<div class="keyword hidden">'+promotion.namePromo.toLowerCase()+'</div>' +
                '</div>' +
                '<div class="text-center background-lightgray p-2">'+
                    '<span>Ver m&aacute;s</span>'+
                '</div>' +
            '</div>' +
        '</div>';
    $('#elements_container').append(row);

    $('.promotion').click(function() {
        openPromotion($(this));
    });
}

function openPromotion(promotion) {
    // Open modal
    $('#promotion_image_store').attr("src", $(promotion).find('.promotion-image-store').attr("src"));
    $('#promotion_name_store').html($(promotion).find('.promotion-name-store').html());
    $('#promotion_image').attr("src", $(promotion).find('.promotion-image').attr("src"));
    $('#promotion_greeting').html($(promotion).find('.promotion-greeting').html());
    $('#promotion_name').html($(promotion).find('.promotion-name').html());
    $('#promotion_start_date').html('Desde: '+$(promotion).find('.promotion-start-date').html());
    $('#promotion_end_date').html('Hasta: '+$(promotion).find('.promotion-end-date').html());
    $('#promotion_title').html($(promotion).find('.promotion-name').html());
    $('#promotion_description').html($(promotion).find('.promotion-description').html());
    var url_redirection = $(promotion).find('.promotion-url-redirection').html();
    $('.modal-promotion').find('.modal-footer').hide();
    if (url_redirection != '') {
        $('#btn_promotion_details').click(function() {
            document.location.href = url_redirection;
        });
        $('.modal-promotion').find('.modal-footer').show();
    }
    $('.modal-promotion').modal();
}

function initIsotope() {
    var $container = $('.elements');

    // init Isotope
    $container.isotope({
        itemSelector: '.element',
        layoutMode: 'fitRows',
        filter: ':not(.initially-hidden)'
    });

    var PageLoadFilter = ':not(.initially-hidden)';
    $container.isotope({filter: PageLoadFilter});

    // layout Isotope after each image loads
    $container.imagesLoaded().progress( function() {
        $container.isotope('layout');
    });

    // filter functions
    $('.filter-text').keyup(function () {
        var filterValue = $(this).val();
        applyIsotopeFilters(filterValue);
    });
    $('.filter-text').change(function () {
        var filterValue = $(this).val();
        applyIsotopeFilters(filterValue);
    });
}

function applyIsotopeFilters(text) {
    if (text == null) {
        text = $('.filter-text').val();
    }
    text = text.toLowerCase().trim();

    var $container = $('.elements');
    $container.isotope({filter: function() {
        if (text == "") {
            return false;
        }
        else {
            var keyword = $(this).find('.keyword').text();
            return keyword.indexOf(text) >= 0;
        }
    }});
}