var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.getPromotion");

$(function() {
    var usuario = getUserData();
    if (usuario.preferencias.categorias.length > 0 || usuario.preferencias.marcas.length > 0) {
        loadNotifications();
        $('#btn_load_more').click(function () {
            loadNotifications();
        });
    }
    else {
        document.location.href = urlClient.concat(usuario.directorio_vistas+"preferences.html");
    }
});

function loadNotifications() {
    var usuario = getUserData();
    var ultima_notification = $('#table_notifications > tbody > tr').length;
    var cantidad_notificaciones = 10;
    var params = {"idUser": usuario.id_usuario, "pointer": ultima_notification, "numberRecords": cantidad_notificaciones};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            if (respuesta.hasOwnProperty('promociones')) {
                for (i=0; i<respuesta.promociones.length; i++) {
                    var notification = respuesta.promociones[i];
                    loadNotification(notification);
                }
                if (respuesta.promociones.length < cantidad_notificaciones) {
                    $('#btn_load_more').hide();
                }
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadNotification(notification) {
    // console.log(JSON.stringify(notification));
    if ($('#table_notifications .'+notification.idPromotion).length <= 0) {
        moment.locale('es');
        var dayStartDate = moment(notification.dateStart).format("DD");
        var monthStartDate = moment(notification.dateStart).format("MMM").toUpperCase();
        var yearStartDate = moment(notification.dateStart).format("YYYY");
        var dayEndDate = moment(notification.dateEnd).format("DD");
        var monthEndDate = moment(notification.dateEnd).format("MMM").toUpperCase();
        var yearEndDate = moment(notification.dateEnd).format("YYYY");

        var url_redirection = 'index.html';
        if (notification.type == "P") {
            var usuario = getUserData();
            url_redirection = urlClient.concat(usuario.directorio_vistas+"store.html?id_store="+notification.idStore);
        }
        var row =
            '<tr class="msg-display mb-2 clickable '+((notification.visto == "N")?'notification-no-readed ':'')+notification.idPromotion+'">' +
                '<td>' +
                    '<div class="notification-id hidden">' +
                        notification.idPromotion +
                    '</div>' +
                    '<div class="notification-greeting hidden">' +
                        notification.greeting +
                    '</div>' +
                    '<div class="notification-name-promotion hidden">' +
                        notification.namePromotion +
                    '</div>' +
                    '<div class="notification-description-promotion hidden">' +
                        notification.description +
                    '</div>' +
                    '<div class="notification-start-date hidden">' +
                        dayStartDate+' '+monthStartDate+' '+yearStartDate +
                    '</div>' +
                    '<div class="notification-end-date hidden">' +
                        dayEndDate+' '+monthEndDate+' '+yearEndDate +
                    '</div>' +
                    '<div class="notification-url-redirection hidden">' +
                        url_redirection +
                    '</div>' +
                    '<div class="media m-0 py fw">' +
                        '<div class="row">' +
                            '<div class="col-2">' +
                                '<img src="'+notification.imageStore+'" alt="'+notification.nameStore+'" class="fw notification-image-store">' +
                            '</div>' +
                            '<div class="col-10 text-left text-md notification-name-store">' +
                                notification.nameStore +
                            '</div>' +
                        '</div>' +
                        '<div class="align-items-end align-self-center">' +
                            '<div class="clickable">' +
                                '<sup><em class="ion-close-round text-muted icon-2x close_notification"></em></sup>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div>' +
                        '<img src="'+notification.imageURL+'" alt="'+notification.namePromotion+'" class="fw notification-image-promotion">' +
                    '</div>' +
                    '<div class="text-center background-lightgray p-2">'+
                        '<span>Ver m&aacute;s</span>'+
                    '</div>' +
                '</td>' +
            '</tr>'
        if ($('#table_notifications > tbody > tr').length > 0) {
            $('#table_notifications > tbody:last-child').append(row);
        }
        else {
            $('#table_notifications > tbody').html(row);
        }
        $('#table_notifications .close_notification').click(function() {
            var row = $(this).closest('tr');
            row.hide();
            // Change the notification status to 'D'
            var usuario = getUserData();
            var id_promotion = $(row).find('.notification-id').html();
            var urlServerPage2 = urlServer.concat("ws/co.openmas.wokemall.setViewPromo");
            var params = {
                "data": JSON.stringify({
                    "idOrg": idOrganization,
                    "idUser": usuario.id_usuario,
                    "idProm": id_promotion,
                    "action": 'D'
                })
            };
            // console.log("DATA TO SEND: "+JSON.stringify(params));
            $.ajax({
                type: "POST",
                url: urlServerPage2,
                data: params,
                dataType:"text",
                username: serverUser,
                password: serverPassword,
                xhrFields: {
                    withCredentials: true
                },
                success: function(respuesta) {
                    var respuesta = JSON.parse(respuesta);
                    console.log("RESPUESTA: "+JSON.stringify(respuesta));
                },
                error: function(respuesta) {
                    console.log("ERROR: "+JSON.stringify(respuesta));
                }
            });
        });
        $('#table_notifications .msg-display.'+notification.idPromotion).on('click', function(e){
            var element = $(e.target);
            if (!element.hasClass('close_notification')) {
                openNotification($(this));
            }
        });
    }
}

function openNotification(row) {
    // Open modal
    $('#notification_image_store').attr("src", $(row).find('.notification-image-store').attr("src"));
    $('#notification_name_store').html($(row).find('.notification-name-store').html());
    $('#notification_image_promotion').attr("src", $(row).find('.notification-image-promotion').attr("src"));
    $('#notification_greeting').html($(row).find('.notification-greeting').html());
    $('#notification_name_promotion').html($(row).find('.notification-name-promotion').html());
    $('#notification_start_date').html($(row).find('.notification-start-date').html());
    $('#notification_end_date').html($(row).find('.notification-end-date').html());
    $('#notification_title_promotion').html($(row).find('.notification-name-promotion').html());
    $('#notification_description_promotion').html($(row).find('.notification-description-promotion').html());
    var url_redirection = $(row).find('.notification-url-redirection').html();
    $('.modal-notification').find('.modal-footer').hide();
    if (url_redirection != '') {
        $('#btn_notification_details').click(function() {
            document.location.href = url_redirection;
        });
        $('.modal-notification').find('.modal-footer').show();
    }
    $('.modal-notification').modal();

    if ($(row).hasClass('notification-no-readed')) {
        var usuario = getUserData();
        var id_promotion = $(row).find('.notification-id').html();
        var urlServerPage2 = urlServer.concat("ws/co.openmas.wokemall.setViewPromo");
        var params = {
            "data": JSON.stringify({
                "idOrg": idOrganization,
                "idUser": usuario.id_usuario,
                "idProm": id_promotion,
                "action": 'V'
            })
        };
        // console.log("DATA TO SEND: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage2,
            data: params,
            dataType:"text",
            username: serverUser,
            password: serverPassword,
            xhrFields: {
                withCredentials: true
            },
            success: function(respuesta) {
                var respuesta = JSON.parse(respuesta);
                // console.log("RESPUESTA: "+JSON.stringify(respuesta));
                if (respuesta.status) {
                    $(row).removeClass('notification-no-readed');
                    // Actualizar los tooltips que muestran la cantidad de mensajes sin leer
                    $('.messages_total').html(respuesta.qtyToView);
                    $('.messages_total').show();
                    if (respuesta.qtyToView <= 0) {
                        $('.messages_total').hide();
                    }
                }
                else {
                    swal(respuesta.message, '', 'error');
                }
            },
            error: function(respuesta) {
                console.log("ERROR: "+JSON.stringify(respuesta));
            }
        });
    }
}

function checkParameters() {
    var id_message = getUrlParameter('id_message');
    if (id_message != '') {
        if ($('#table_notifications .msg-display.'+id_message).length > 0) {
            //console.log("CHECK PARAMETERS: "+id_message);
            $('#table_notifications .msg-display.'+id_message).click();
        }
    }
}