var urlServerPage = urlServer.concat("ws/co.openmas.wokemall.userValidated");

(function() {
    'use strict';
    $(userSignin);
})();

/**************************************************************************
 * VALIDACION DE FORMULARIO DE LOGIN - INICIO
 **************************************************************************/
function userSignin() {

    if (!$.fn.validate) return;

    var $form = $('#user-login');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            username: {
                required: true,
                email: true,
                rangelength: [6, 50]
            },
            password: {
                required: true,
                rangelength: [6, 20]
            }
        },
        submitHandler: function() {
            login();
        }
    });
}

function login() {
    var user = {};
    user.mail = $('#username').val();
    user.password = hex_md5($('#password').val());
    var params = {"userMail": user.mail, "userPass": user.password};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            console.log("RESPUESTA: "+JSON.stringify(respuesta));
            if (respuesta.userValid) {
                var usuario = {
                    "id_usuario": respuesta.id,
                    "correo": respuesta.email,
                    "nombres": respuesta.name,
                    "apellidos": respuesta.lastName,
                    "nombre_completo": respuesta.name+" "+respuesta.lastName,
                    "fecha_nacimeinto": respuesta.birthDate,
                    "imagen": urlClient.concat('resources/img/user.jpg'),
                    "directorio_vistas": "visitor/",
                    "preferencias": getPreferencesFormat(respuesta.preference)
                }
                setUserData(usuario);
                document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
            }
            else {
                swal('Combinacion de Correo y Clave incorrecta', '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * VALIDACION DE FORMULARIO DE LOGIN - FIN
 **************************************************************************/

function getPreferencesFormat(preferences) {
    var result = {
        categorias: [],
        marcas: []
    };
    for (i=0; i<preferences.length; i++) {
        var preference = preferences[i];
        if (preference.idCategory != "" && preference.idCategory != "null" && preference.idCategory != null) {
            result.categorias.push(preference.idCategory);
        }
        else if (preference.idBrand != "" && preference.idBrand != "null" && preference.idBrand != null) {
            result.marcas.push(preference.idBrand);
        }
    }
    return result;
}