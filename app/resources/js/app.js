/*!
 *
 * Dasha - Bootstrap Admin Template
 *
 * Version: 1.3
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 *
 */

/**************************************************************************
 * GESTION DE PETICIONES AJAX - INICIO
 **************************************************************************/
$(document).on({
    ajaxStart: function () {
        $("body").addClass("loading");
    },
    ajaxStop: function () {
        $("body").removeClass("loading");
    }
});

(function() {
    'use strict';

    // Disable warning "Synchronous XMLHttpRequest on the main thread is deprecated.."
    $.ajaxPrefilter(function(options) {
        options.async = true;
    });

    // used for the preloader
    $(function() { document.body.style.opacity = 1; });

})();
/**************************************************************************
 * GESTION DE PETICIONES AJAX - FIN
 **************************************************************************/

/**************************************************************************
 * INICIALIZACION DE DATOS AL CULMINAR LA CARGA DE LAS PAGINAS - INICIO
 **************************************************************************/
$(function() {
    // Agregar el div usado para aplicar la clase loading
    addLoadingDiv();

    // Colocar el nombre de aplicacion donde este configurado
    loadApplicationName();

    // Colocar los datos del usuario donde este configurado
    loadUserData($(this));

    // Agregar elementos especificos de un usuario
    var usuario = getUserData();
    if (usuario != null) {
        // Aplicar estilo segun el rol
        addBodyStyles(usuario);

        // Cargar los archivos HTML a incluir (Inclusion Interna)
        loadInnerHTML();

        // Buscar nuevas notificaciones
        searchNewNotifications();
        setInterval(searchNewNotifications, timeoutLoadNewNotifications);
    }
});

function addLoadingDiv() {
    if ($('body .modal-loading').length == 0) {
        // Para la animacion de carga usando CSS
        var bars = (loadingAnimationBars != undefined)?loadingAnimationBars:8;
        var modal = '<div class="modal-loading"><div class="spinner">';
        for (i=0; i<bars; i++) {
            modal += '<i></i>';
        }
        modal += '</div></div>';
        $('body').append(modal);
    }
}

function loadApplicationName() {
    $.each($('.application-name'), function(i, element) {
        $(element).html(applicationName+$(element).html());
    });
}

function loadUserData(element) {
    // Cargar el email del usuario en algunos elementos
    if ($(element).find('.user_email').length > 0) {
        $.each($(element).find('.user_email'), function(i, element2) {
            $(element2).html(usuario.correo);
            $(element2).val(usuario.correo);
        });
    }

    // Cargar el nombre del usuario en algunos elementos
    if ($(element).find('.user_firstname').length > 0) {
        $.each($(element).find('.user_firstname'), function(i, element2) {
            $(element2).html(usuario.nombres);
            $(element2).val(usuario.nombres);
        });
    }

    // Cargar el apellido del usuario en algunos elementos
    if ($(element).find('.user_lastname').length > 0) {
        $.each($(element).find('.user_lastname'), function(i, element2) {
            $(element2).html(usuario.apellidos);
            $(element2).val(usuario.apellidos);
        });
    }

    // Cargar el nombre completo del usuario en algunos elementos
    if ($(element).find('.user_fullname').length > 0) {
        $.each($(element).find('.user_fullname'), function(i, element2) {
            $(element2).html(usuario.nombre_completo);
            $(element2).val(usuario.nombre_completo);
        });
    }
}

function addBodyStyles(usuario) {
    $('body').addClass('footer-fixed');
    if (usuario.directorio_vistas.indexOf('visitor') >= 0 ) {
        $('body').addClass('theme-default');
    }
}

function loadInnerHTML() {
    $.each($('.includeHTML'), function(i, element) {
        if (element.hasAttribute('data-html')) {
            $(element).load($(element).attr('data-html'), function() {
                // Colocar los datos del usuario donde este configurado
                loadUserData(element);

                // Cargar el telefono del usuario en algunos elementos
                if ($(element).find('.user_phone').length > 0) {
                    $.each($(element).find('.user_phone'), function(i, element2) {
                        $(element2).html(usuario.telefono);
                        $(element2).val(usuario.telefono);
                    });
                }

                // Cargar la imagen del usuario en algunos elementos
                if ($(element).find('.user_image').length > 0) {
                    $.each($(element).find('.user_image'), function(i, element2) {
                        $(element2).attr("src", usuario.imagen);
                    });
                }

                // Cargar el titulo de la ventana en algunos elementos
                if ($(element).find('.view_title').length > 0 && element.hasAttribute('data-title')) {
                    $.each($(element).find('.view_title'), function(i, element2) {
                        $(element2).html($(element2).html()+$(element).attr('data-title'));
                    });
                }

                // Cargar la funcionalidad del Menu (SideBar)
                if ($(element).find('.sidebar-nav').length > 0) {
                    (function () {
                        'use strict';

                        $(sidebarNav);

                        function sidebarNav() {
                            var $sidebarNav = $('.sidebar-nav');
                            var $sidebarContent = $('.sidebar-content');

                            activate($sidebarNav);

                            $sidebarNav.on('click', function (event) {
                                var item = getItemElement(event);
                                // check click is on a tag
                                if (!item) return;

                                var ele = $(item),
                                    liparent = ele.parent()[0];

                                var lis = ele.parent().parent().children(); // markup: ul > li > a
                                // remove .active from childs
                                lis.find('li').removeClass('active');
                                // remove .active from siblings ()
                                $.each(lis, function (idx, li) {
                                    if (li !== liparent)
                                        $(li).removeClass('active');
                                });

                                var next = ele.next();
                                if (next.length && next[0].tagName === 'UL') {
                                    ele.parent().toggleClass('active');
                                    event.preventDefault();
                                }
                            });

                            // find the a element in click context
                            // doesn't check deeply, asumens two levels only
                            function getItemElement(event) {
                                var element = event.target,
                                    parent = element.parentNode;
                                if (element.tagName.toLowerCase() === 'a') return element;
                                if (parent.tagName.toLowerCase() === 'a') return parent;
                                if (parent.parentNode.tagName.toLowerCase() === 'a') return parent.parentNode;
                            }

                            function activate(sidebar) {
                                sidebar.find('a').each(function () {
                                    var $this = $(this),
                                        href = $this.attr('href').replace('#', '');
                                    if (href !== '' && window.location.href.indexOf('/' + href) >= 0) {
                                        var item = $this.parents('li').addClass('active');
                                        // Animate scrolling to focus active item
                                        $sidebarContent.animate({
                                            scrollTop: $sidebarContent.scrollTop() + item.position().top - (window.innerHeight / 2)
                                        }, 100);
                                        return false; // exit foreach
                                    }
                                });
                            }

                            var layoutContainer = $('.layout-container');
                            var $body = $('body');
                            // Handler to toggle sidebar visibility on mobile
                            $('.sidebar-toggler').click(function (e) {
                                e.preventDefault();
                                layoutContainer.toggleClass('sidebar-visible');
                                // toggle icon state
                                $(this).parent().toggleClass('active');
                            });
                            // Close sidebar when click on backdrop
                            $('.sidebar-layout-obfuscator').click(function (e) {
                                e.preventDefault();
                                $body.removeClass('sidebar-cover-visible'); // for use with cover mode
                                layoutContainer.removeClass('sidebar-visible'); // for use on mobiles
                                // restore icon
                                $('.sidebar-toggler').parent().removeClass('active');
                            });

                            // escape key closes sidebar on desktops
                            $(document).keyup(function (e) {
                                if (e.keyCode === 27) {
                                    $body.removeClass('sidebar-cover-visible');
                                }
                            });

                            // Handler to toggle sidebar visibility on desktop
                            $('.covermode-toggler').click(function (e) {
                                e.preventDefault();
                                $body.addClass('sidebar-cover-visible');
                            });

                            $('.sidebar-close').click(function (e) {
                                e.preventDefault();
                                $body.removeClass('sidebar-cover-visible');
                            });

                            // remove desktop offcanvas when app changes to mobile
                            // so when it returns, the sidebar is shown again
                            window.addEventListener('resize', function () {
                                if (window.innerWidth < 768) {
                                    $body.removeClass('sidebar-cover-visible');
                                }
                            });

                        }

                    })();

                    $('.messages_total').hide();
                }

                // Colocar el nombre de aplicacion donde este configurado
                if ($(element).find('.application-name').length > 0) {
                    $.each($(element).find('.application-name'), function(i, element2) {
                        $(element2).html(applicationName+$(element2).html());
                    });
                }

                // Ejecutar la funcion init() si esta definida
                if ($(element).hasClass('executeInitFunction')) {
                    if( typeof init !== 'undefined' && jQuery.isFunction( init ) ) {
                        //Es seguro ejecutara la función
                        init();
                    }
                }
            });
        }
    });
}

function searchNewNotifications() {
    var usuario = getUserData();
    var urlServerPage2 = urlServer.concat("ws/co.openmas.wokemall.getNumPromViewed");
    var params = {
        "idUser": usuario.id_usuario
    };
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage2,
        data: params,
        dataType:"text",
        username: serverUser,
        password: serverPassword,
        xhrFields: {
            withCredentials: true
        },
        success: function(respuesta) {
            var respuesta = JSON.parse(respuesta);
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            if (respuesta.hasOwnProperty('qtyToView')) {
                // Actualizar los tooltips que muestran la cantidad de mensajes sin leer
                $('.messages_total').html(respuesta.qtyToView);
                $('.messages_total').show();
                if (respuesta.qtyToView <= 0) {
                    $('.messages_total').hide();
                }
            }
            else {
                console.log("EXITO FALSE: "+JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * INICIALIZACION DE DATOS AL CULMINAR LA CARGA DE LAS PAGINAS - FIN
 **************************************************************************/

/**************************************************************************
 * GESTION DE EVENTOS DE ELEMENTOS GENERICOS EN LAS PAGINAS - INICIO
 **************************************************************************/
$(document).on('click', '.close_session', function(e) {
    e.preventDefault();
    closeSession();
});

$(document).on('click', '.user_dashboard', function(e) {
    e.preventDefault();
    var page = "index.html";
    document.location.href = urlClient.concat(usuario.directorio_vistas+page);
});

$(document).on('click', '.user_notifications', function(e) {
    e.preventDefault();
    var page = "notifications.html";
    document.location.href = urlClient.concat(usuario.directorio_vistas+page);
});

$(document).on('click', '.user_profile', function(e) {
    e.preventDefault();
    // var page = "profile.html";
    // document.location.href = urlClient.concat(usuario.directorio_vistas+page);
});

$(document).on('click', '.user_preferences', function(e) {
    e.preventDefault();
    var page = "preferences.html";
    document.location.href = urlClient.concat(usuario.directorio_vistas+page);
});

$(document).on('click', '.what_looking', function(e) {
    e.preventDefault();
    var page = "what_looking_today.html";
    document.location.href = urlClient.concat(usuario.directorio_vistas+page);
});

$(document).on('click', 'a > .history-back', function(e) {
    e.preventDefault();
    window.history.back();
});

$(document).on('click', '.custom-button', function(e) {
    e.preventDefault();
    var others = $(this).closest('.custom-button-group').find('.selected');
    if (others.length > 0) {
        others.removeClass('selected');
        if ($(others).find('input:checkbox').length > 0) {
            $(others).find('input:checkbox').prop('checked',false);
        }
    }
    if ($(this).find('input:checkbox').length > 0) {
        if ($(this).find('input:checkbox').is(':checked')) {
            $(this).removeClass('selected');
            $(this).find('input:checkbox').prop('checked',false);
        }
        else {
            $(this).addClass('selected');
            $(this).find('input:checkbox').prop('checked',true);
        }
    }
});
/**************************************************************************
 * GESTION DE EVENTOS DE ELEMENTOS GENERICOS EN LAS PAGINAS - FIN
 **************************************************************************/