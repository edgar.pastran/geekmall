/**************************************************************************
 * DEFINICION DE VARIABLES GLOBALES - INICIO
 **************************************************************************/
// VARIABLES ASIGNADAS
// Nombre de la aplicacion
var applicationName = "ConectBuy";
// Cantidad de barras usadas en la animacion de carga
// Se usa con el CSS loading.css
var loadingAnimationBars = 3;
// Tiempo en que se consulta al servidor por nuevas notificaciones
var timeoutLoadNewNotifications = 5 * 60 * 1000;

// VARIABLES CALCULADAS
var IPServer = "52.9.177.54";
// var IPServer = "www.conectbuy.com";
var urlBase = window.location.protocol + "//";
if (!window.location.protocol.startsWith("file")) {
    urlBase += window.location.host + "/";
    if ((window.location.host == "www.conectbuy.com") ||
        (window.location.host == IPServer) ||
        (window.location.host == "localhost") ||
        (window.location.host.startsWith("10."))) {
        urlBase += window.location.pathname.split('/')[1] + "/";
    }
    var urlClient = urlBase.concat("app/");
}
else {
    var directory = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/')+1);
    var urlClient = urlBase.concat(directory);
}
// console.log("URL BASE : "+urlBase);
// console.log("URL CLIENT : "+urlClient);
var urlServer = "http://"+IPServer+"/openbravo/";
// console.log("URL SERVER : "+urlServer);
var serverUser = "Openbravo";
// console.log("SERVER USER: "+serverUser);
var serverPassword = "openbravo";
// console.log("SERVER PASSWORD: "+serverPassword);
var idOrganization = "2E60544D37534C0B89E765FE29BC0B43";
// console.log("SERVER PASSWORD: "+serverPassword);
var currentURL = urlBase.concat(window.location.pathname.substring(window.location.pathname.substring(1).indexOf("/")+2));
// console.log("CURRENT URL : "+currentURL);
var currentPage = currentURL.substring(currentURL.lastIndexOf('/')+1);
// console.log("CURRENT PAGE : "+currentPage);
/**************************************************************************
 * DEFINICION DE VARIABLES GLOBALES - FIN
 **************************************************************************/

/**************************************************************************
 * VALIDACION DE PRIVILEGIOS DE ACCESO DE USUARIO - INICIO
 **************************************************************************/
var usuario = getUserData();
// console.log("USUARIO: "+JSON.stringify(usuario));
var pagesWithoutUser = [
    "login.html",
    "recover.html",
    "signup.html",
    "verification.html",
    "resend.html"
];
if (usuario == null) {
    if (pagesWithoutUser.indexOf(currentPage) < 0) {
        document.location.href = urlClient.concat("login.html");
    }
}
else if (currentURL.indexOf(usuario.directorio_vistas) == -1) {
    document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
}
else if (urlClient.indexOf(usuario.directorio_vistas) >= 0) {
    urlClient = urlClient.replace(usuario.directorio_vistas, "");
}
/**************************************************************************
 * VALIDACION DE PRIVILEGIOS DE ACCESO DE USUARIO - FIN
 **************************************************************************/

/**************************************************************************
* DEFINICION DE FUNCIONES PARA LA GESTION DE VARIABLES DE SESION - INICIO
**************************************************************************/
function setSessionData(name, data) {
    window.localStorage.setItem(name, JSON.stringify(data));
}

function getSessionData(name) {
    var data = window.localStorage.getItem(name);
    return (data != undefined)?JSON.parse(data):null;
}

function closeSession() {
    window.localStorage.clear();
    document.location.href = urlClient.concat("login.html");
};

function getUserData() {
    return getSessionData(applicationName.concat('-user'));
}

function setUserData(data) {
    setSessionData(applicationName.concat('-user'), data);
}
/**************************************************************************
 * DEFINICION DE FUNCIONES PARA LA GESTION DE VARIABLES DE SESION - FIN
 **************************************************************************/
